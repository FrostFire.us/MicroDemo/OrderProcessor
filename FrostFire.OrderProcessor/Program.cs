﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using FrostFire.Models;
using FrostFire.OrderProcessor.Entities;
using FrostFire.OrderProcessor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using StackExchange.Redis;
using Order = FrostFire.Models.Order;


namespace FrostFire.OrderProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            new Worker(builder.Build()).Run();
        }
    }

    public class Worker
    {
        private readonly IConfigurationRoot _configuration;
        private OrderContext _ctx;
        private List<OrderDtoOut> _orders;
        private ConnectionMultiplexer _mq;


        public Worker(IConfigurationRoot Configuration)
        {
            _configuration = Configuration;

        }

        public void Run()
        {
            Console.WriteLine("Initializing...");
            PrimeAutoMapper();
            PrimeDB();
            PrimeMQ();
            Console.WriteLine("Running...");
            Console.ReadKey();
        }

        private void PrimeAutoMapper()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Order, OrderDtoOut>();
                cfg.CreateMap<Customer, CustomerDtoOut>();
                cfg.CreateMap<OrderItem, OrderItemDtoOut>();
            });
        }

        private void PrimeDB()
        {
            _ctx = new OrderContext(_configuration["DBServer"], _configuration["DBUsername"], _configuration["DBPassword"]);
            _orders = _ctx.Orders.ToList();
        }

        private void PrimeMQ()
        {
            _mq = ConnectionMultiplexer.Connect(_configuration["MQServer"]);
            _mq.GetSubscriber().Subscribe(_configuration["MQChannel"], Process);
        }

        private void Process(RedisChannel Channel, RedisValue Value)
        {
            Process();
        }

        private void Process()
        {
            string result;
            while (!string.IsNullOrWhiteSpace(result = _mq.GetDatabase().ListRightPop(_configuration["MQList"])))
            {
                OrderDtoIn o = JsonConvert.DeserializeObject<OrderDtoIn>(result);
                _ctx.Add(AutoMapper.Mapper.Map<OrderDtoOut>(o));
                _ctx.SaveChanges();
                Console.WriteLine($"[{_ctx.Orders.Count()}] : {o.Processor} Processed order from {o.Receiver} : {o.OrderID}");
                Thread.Sleep(int.Parse(_configuration["ProcessInterval"]));
            }
        }

    }
}
