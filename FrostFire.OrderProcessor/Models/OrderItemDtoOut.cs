﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using FrostFire.Models;

namespace FrostFire.OrderProcessor.Models
{
    [Table("OrderItems")]
    public class OrderItemDtoOut : OrderItem
    {
        [Column("ID", Order = 0)]
        public int ID { get; set; }

        public string OrderID { get; set; }
    }
}
