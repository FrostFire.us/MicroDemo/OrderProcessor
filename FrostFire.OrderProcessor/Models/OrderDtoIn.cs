﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using FrostFire.Models;

namespace FrostFire.OrderProcessor.Models
{

    public class OrderDtoIn: Order
    {
        public string Receiver { get; set; }
        public string Processor => Environment.MachineName;
    }
}
