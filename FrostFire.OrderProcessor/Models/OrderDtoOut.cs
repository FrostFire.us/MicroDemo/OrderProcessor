﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using FrostFire.Models;

namespace FrostFire.OrderProcessor.Models
{
    [Table("Orders")]
    public class OrderDtoOut: Order
    {
        [Key]
        [Column("OrderID")]
        public new string OrderID { get; set; }
        public new CustomerDtoOut Customer { get; set; }
        public new IEnumerable<OrderItemDtoOut> Items { get; set; }
    }
}
