﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using FrostFire.Models;

namespace FrostFire.OrderProcessor.Models
{
    [Table("Customers")]
    public class CustomerDtoOut: Customer
    {
        [Key]
        [Column("ID", Order=0)]
        public int ID { get; set; }
    }
}
