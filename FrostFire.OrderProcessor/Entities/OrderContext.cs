﻿using System;
using System.Collections.Generic;
using System.Text;
using FrostFire.OrderProcessor.Models;
using Microsoft.EntityFrameworkCore;

namespace FrostFire.OrderProcessor.Entities
{
    public sealed class OrderContext : DbContext
    {
        private readonly string _server;
        private readonly string _username;
        private readonly string _password;

        public OrderContext(string Server, string Username, string Password)
        {
            _server = Server;
            _username = Username;
            _password = Password;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer($"Server={_server};User={_username};Password={_password};Database=FrostFire");
        }

        public DbSet<OrderDtoOut> Orders { get; set; }

    }
}
